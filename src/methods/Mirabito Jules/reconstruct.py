"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np


from colour_demosaicing import (
    demosaicing_CFA_Bayer_bilinear,
    demosaicing_CFA_Bayer_Malvar2004,
    demosaicing_CFA_Bayer_Menon2007,
)


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """

    #The code implemented here comes from an open-source python library that has optimized the two methods presented.
    #This library is based on the "color" library, specific to color image processing.
    #The source code for the following functions can be found in the "demosaicing" folder in the following git hub: https://github.com/colour-science/colour-demosaicing
    #Copyright 2015 Colour Developers -

    if cfa == 'bayer' : 
        #res = demosaicing_CFA_Bayer_bilinear(y,'GRBG')
        res = demosaicing_CFA_Bayer_Menon2007(y,'GRBG')
        #res = demosaicing_CFA_Bayer_Malvar2004(y,'GRBG')
    else : 
        print('Error - Not implemented')
        return

    return res

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
