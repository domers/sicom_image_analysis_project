"""A file containing some useful checks for the project.
This file should NOT be modified.
"""


from os.path import exists
import numpy as np



def check_path(file_path: str) -> None:
    """Checks if a file exists at file_path and is a png image.

    Args:
        file_path (str): Path to check

    Raises:
        Exception: Exception if the path is invalid.
    """
    if not exists(file_path):
        raise Exception('File does not exist.')


def check_png(file_path: str) -> None:
    """Checks if the file is a png image.

    Args:
        file_path (str): Path to check.
    """
    if not file_path.endswith('.png'):
        raise Exception(f'Path must end with ".png". Got {file_path[-4:]}.')


def check_rgb(img: np.ndarray) -> None:
    """Checks if image is a 3 dimensional array with 3 channels.

    Args:
        img (np.ndarray): Image to check.

    Raises:
        Exception: Exception if image is not a 3 dimensional array with 3 channels.
    """
    if not (len(img.shape) == 3 and img.shape[2] == 3):
        raise Exception(f'The images must be 3 dimensional (RGB) arrays. Got an array of shape {img.shape}.')


def check_shape(img1: np.ndarray, img2: np.ndarray) -> None:
    """Checks if img1 and img2 have the same shape.

    Args:
        img1 (np.ndarray): First image.
        img2 (np.ndarray): Second image.

    Raises:
        Exception: Exception if img1 and img2 do not have the same shape.
    """
    if img1.shape != img2.shape:
        raise Exception(f'The images must have the same shape. Got {img1.shape} and {img2.shape}.')


def check_data_range(img: np.ndarray) -> None:
    """Checks if the values of img are in the interval [0, 1].

    Args:
        img (np.ndarray): Image to check.

    Raises:
        Exception: Exception if img's values are not in [0, 1].
    """
    if np.max(img) > 1 or np.min(img) < 0:
        raise Exception(f'Pixel\'s values must be in range [0, 1]. Got range [{np.min(img)}, {np.max(img)}].')


def check_cfa(cfa: str) -> None:
    """Checks if the CFA's name is correct.

    Args:
        cfa (str): CFA name.

    Raises:
        Exception: Exception if the name of the CFA is not correct.
    """
    if cfa not in ['bayer', 'quad_bayer']:
        raise Exception(f'Unknown CFA name. Got {cfa} but expected either bayer or quad_bayer.')


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
